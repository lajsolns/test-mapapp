package com.example.mapapp3

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    var mapReady: Boolean = false
    lateinit var btnAccra: Button
    lateinit var btnTaadi: Button
    lateinit var btnKumasi: Button

    //        Takoradi locations
    val firstSamule = MarkerOptions()
        .position(LatLng(4.898721, -1.764412))
        .title("first Samuel")
    val BethelMethodist = MarkerOptions()
        .position(LatLng(4.894791, -1.759771))
        .title("bethel methodist")

    //        Accra locations
    val agbogbloshi = MarkerOptions()
        .position(LatLng(5.552422, -0.213776))
        .title("agbogbloshi")

    val accraBreweries = MarkerOptions()
        .position(LatLng(5.553951, -0.216654))
        .title("Accra Breweries")

    //      Kumasi locations
    val katanga = MarkerOptions()
        .position(LatLng(6.672556, -1.572622))
        .title("katanga")

    val paaJoe = MarkerOptions()
        .position(LatLng(6.677052, -1.569825))
        .title("paaJoe")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        btnAccra = findViewById(R.id.btnAccra)
        btnTaadi = findViewById(R.id.btnTaadi)
        btnKumasi = findViewById(R.id.btnKumasi)

        btnKumasi.setOnClickListener {

            if (mapReady) {
                mMap.animateCamera(
                    CameraUpdateFactory.newCameraPosition(
                        CameraPosition.builder()
                            .target(LatLng(6.677052, -1.569825))
                            .zoom(16F)
                            .bearing(0F)
                            .tilt(45F)
                            .build()
                    ), 3000, null
                )
            }
        }

        btnTaadi.setOnClickListener {
            if (mapReady) {

                mMap.animateCamera(
                    CameraUpdateFactory.newCameraPosition(
                        CameraPosition.builder()
                            .target(LatLng(4.894791, -1.759771))
                            .zoom(16F)
                            .bearing(0F)
                            .tilt(45F)
                            .build()
                    ), 3000, null
                )
            }
        }

        btnAccra.setOnClickListener {
            if (mapReady) {
                mMap.animateCamera(
                    CameraUpdateFactory.newCameraPosition(
                        CameraPosition.builder()
                            .target(LatLng(5.553951, -0.216654))
                            .zoom(16F)
                            .bearing(0F)
                            .tilt(45F)
                            .build()
                    ), 3000, null
                )
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mapReady = true
        mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
        mMap.addMarker(agbogbloshi)
        mMap.addMarker(accraBreweries)
        mMap.addMarker(katanga)
        mMap.addMarker(paaJoe)
        mMap.addMarker(firstSamule)
        mMap.addMarker(BethelMethodist)

        mMap.addCircle(
            CircleOptions()
                .center(LatLng(4.894791, -1.759771))
                .radius(500.0)
                .strokeColor(Color.RED)
                .fillColor(Color.argb(64, 0, 255, 0))
        )


        mMap.animateCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.builder()
                    .target(LatLng(6.6666, -1.6163))
                    .zoom(16F)
                    .bearing(0F)
                    .tilt(45F)
                    .build()
            )
        )
    }
}
